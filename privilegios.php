<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Club de Privilegios</title>
	<?php include('contenido/head.php'); ?>
</head>

<body>

	<?php include('chat.php'); ?>
 
	<div id="container">
		<?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?>

		<div id="content">
 
			<div class="page-banner">         

				<div class="container">
					<h2>Club de privilegios</h2>
				</div>
			</div>

			<div class="about-box">
				 
		<div class="section">
			<div id="about-section">

				<div class="welcome-box">
					<div class="container">
 
						<p align="justify">Es el primer y único programa de lealtad en la industria automotriz en México que tiene como objetivo crear en la mente del consumidor una imagen de calidad y servicio inigualables que reconoce y recompensa su preferencia por la marca a lo largo del tiempo.</p><br>
                 
<p align="left"><strong>Servicios Preferenciales</strong></p><br>   
<p align="justify">
- Horarios de servicio extendido para la recepción y entrega de su automóvil<br>
- Pruebas de manejo de nuevos modelos a domicilio y en horarios preferenciales en la agencia<br>
- Línea exclusiva de atención telefónica para cualquier asunto relacionado con el programa<br><br>
<strong>Club de Privilegios:</strong> 01 800 527 46 63<br>
- Invitación preferencial a eventos y presentaciones
- Promociones exclusivas
</p><br><br>

<p align="left"><strong>Beneficios del Club de Privilegios</strong> Para que usted disfrute de los premios del Club de Privilegios deberá cumplir con los siguientes requisitos:</p><br>   
<p align="justify">
- Presentar identificación personal<br>
- La tarjeta de membresía Club de privilegios y proporcionar su NIP<br>
- Los puntos solamente pueden ser usados por el titular de la cuenta<br><br>
- Se canjean en múltiplos de 100 puntos
- Los puntos se pueden canjear en cualquier Concesionaria participante<br>
- Cada punto que se otorga equivale lo mismo en pesos
</p><br><br>

<p align="left"><strong>Horarios de atención</strong></p><br>   
<p align="justify">
- Lunes a Viernes.  8:30 a 19:00 hrs<br>
- Sábados.  9:00 a 14:00 hrs<br><br></p>

                	 </div>
					 </div>
		         </div>
		     </div>
	     </div>
    </div>
    
	<?php include('contenido/footer.php'); ?>

</body>
</html>