<!doctype html>
<html lang="es" class="no-js">
<head>
	<title>Autofinanciamiento</title>
	<?php include('contenido/head.php'); ?>
</head>
<body> 

	<?php include('chat.php'); ?>
 
	<div id="container">
		 <?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?>
		 
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Auto Financiamiento</h2>

				</div>
			</div>

			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
					<p align="center">	
                    
             <span class="single-project-content">
                    <img alt="Auto Financiamiento" src="images/autofinanciamiento.png"></span>
             
             </p><br>
						
                        <div class="col-md-11" align="center">
						  <h3 align="left">¿Qué es el Autofinanciamiento?</h3>
<p align="justify" align="center">Es un sistema que te permite, tanto a personas físicas o morales, programar la compra de un automóvil ultimo modelo de la marca HONDA.</p><br>

						<h3 align="left">¿Cómo funciona?</h3>
<p align="justify">Consiste en la integración de  grupos de personas, que a través de pagar sus mensualidades puntualmente, tienen el derecho de participar en un Sorteo mensual que les permite adquirir desde su primera mensualidad un automóvil nuevo.<br>
 Además tiene la ventaja que puede adquirirse al auto nuevo a través de otros medios de asignación como:<br><br>
 <p align="justify">•	<strong>Subasta</strong> - Consiste en ofrecer un número determinado de mensualidades por el cliente, para tener la posibilidad de que si es el cliente que más mensualidades ofreció obtendrá el derecho de adquirir su automóvil nuevo.<br>
•	<strong>Orden secuencial </strong>- Consiste en que si después de designar el ganador de sorteo y no hay ofrecimiento de mensualidades por parte de algún integrante del grupo y este cuenta con los recursos suficientes para designar otra unidad se utiliza el orden secuencial del sorteo y se le asigna al cliente que este al corriente en sus pagos y que no tenga la unidad<br>

 <p align="justify"> Actualmente<strong> Autofinanciamiento HONDA</strong> cuenta con planes de entrega garantizada a corto y mediano plazo, que no dependen de Sorteo o subasta abierta.</p><br>
 
 <h3 align="left">B2 Programa tu compra</h3>
 
 <p align="justify">Está diseñado para personas que quieren programar la compra de su automóvil <strong>HONDA.</strong><br>
El clásico promocional de 48 mensualidades se entregan los automóviles vía sorteo y por medio de subastas cerradas no garantizadas que te permiten subastar la cantidad que quieras y esperar a ver el resultado de tu grupo y ver si resultaste ganador para así poder llevarte tu automóvil. <br><br>
• El Plan de Autofinanciamiento más económico del mercado.<br>
• Subasta cerrada.<br>
• Tienes la posibilidad de obtener tu HONDA desde la primera mensualidad.<br>
• Sin Cuota de Inscripción, sin intereses, sin anualidades y sin enganche. <br>
• 48 Mensualidades.<br><br>


<h3 align="left">B4 Tu puntualidad te premia</h3>

<p align="justify">Está diseñado para personas que quieren programar la compra de su automóvil HONDA y obtener beneficios por pagos puntuales.<br>
Los beneficios de los que usted puede gozar al suscribirse en B4 son:<br><br>
• Por cada 11 pagos puntuales y consecutivos Autofinanciamiento HONDA bonificara una mensualidad del final del plazo.<br>
• Tiene un excelente costo financiero si pagan puntualmente.</p><br><br>

<h3 align="left">BG Garantizado Adjudíquese cuando quiera (3+9)</h3>

<p align="justify">Está diseñado para personas que quieren programar la compra de su automóvil HONDA en un periodo de tres meses (a partir de que quede integrado a un grupo) o máximo al mes 11 con una mensualidad de subasta. <br><br>

En el Garantizado 3+9 de 48 mensualidades se entregan los automóviles vía sorteo y por medio de subastas garantizadas que te permiten programar la entrega del vehículo a partir del tercer mes ofreciendo una subasta que complete 12 mensualidades. <br><br>
    
    
    <h1> Cotiza tu HONDA </h1><br><br>
        
  <div class="about-box">
				 <div class="container">
				 	 <div class="col-md-3" align="center">
					 </div>     	 
                     <div class="col-md-6" align="center">
							 <?php include('form.php'); ?>
					 </div>     	 
			     </div>
			 </div>

					</div>
				</div>
			</div>

		</div><br><br><br><br><br>



		<!-- End content -->


		</div> 

		<br><br><br><br><br><br><br>

			<?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>