<!doctype html>
<html lang="es" class="no-js">
<head>
	<title>Refacciones</title>
	<?php include('contenido/head.php'); ?>
</head>

 <body>

 	<?php include('chat.php'); ?>
 
	<div id="container">
 
		<?php include('contenido/header.php'); ?>
		 <?php include('contenido/analytics.php'); ?> 

		<div id="content">
			<div class="page-banner">         
				<div class="container">
					<h2>Refacciones y Accesorios</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                 <div class="col-md-3" align="center">
					 </div>     	 
                     <div class="col-md-6" align="center">
							 <?php include('form.php'); ?>
					 </div>     	 
			     </div>
			 </div>

		<div class="section">
			<div id="about-section">
				<div class="welcome-box">
					<div class="container">
                
						<h1><span>Partes Originales</span></h1><br>
						<p align="justify">Atienda su HONDA como se merece, le ofrecemos refacciones originales a los mejores precios del mercado. Contamos con un extenso surtido de refacciones originales que nos permiten ofrecerle siempre lo que usted necesita.</p><br>
                 
<p align="left">Proveemos y comercializamos las refacciones y accesorios originales ya sea por mostrador o a través del Taller de Servicio.</p><br><br>

   
<p align="justify"><strong>Los beneficios de adquirir sus refacciones con nosotros son:</strong></p><br>

<p align="justify">- Partes 100% originales con garantía<br>
- Amplio inventario de partes<br>
- Accesorios originales autorizados por HONDA para no perder tu garantía<br>
- Mayor resistencia al desgaste<br>
- Precios altamente competitivos para nuestros clientes<br>
- Excelente Atención<br>
- Promociones durante todo el año<br>
- Servicio de envío por paquetería<br>
</p><br><br>

<p align="justify">Vale la pena invertir en refacciones originales, ya que representan significativos ahorros a largo plazo y mejor funcionamiento de su vehículo.</p><br><br>


<p align="left"><strong>Horarios de atención</strong></p><br>   
<p align="justify">
- Lunes a Viernes.  8:00 a 18:00 hrs.<br>
- Sábados.  8:00 a 13:00 hrs.<br><br></p><br>


                	</div>
				     </div>
                 </div>
             </div>
         </div>
	 </div>

 <?php include('contenido/footer.php'); ?>	 
	 
</body>
</html>