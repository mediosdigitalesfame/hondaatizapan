<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Contacto</title>
	<?php include('contenido/head.php'); ?>
</head>

<body>

	<?php include('chat.php'); ?>

	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>

		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Contáctanos</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-6" align="center">

							<div class="container">
								<div class="col-md-12" >
									<a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">
										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">
										</i> <font size="6"> WhatsApp</font> 
									</button>
								</a>
							</div>
						</div>

						<br>

						<div class="container">
							<div class="col-md-12" >
								<?php include('form.php'); ?>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Circuito Ruiz Cortines #353.</span> <span>Lote 10, Manzana 1. </span><span>Colonia Las Margaritas. CP 52977.</span><span> Atizapán de Zaragoza, Estado de México.</span></li>
								<li><span><i class="fa fa-phone"></i>(55) 1668 9252</span></li>
								<h3>Whatsapp</h3>

								<li>
									<span>
										<i class="fa fa-whatsapp"></i>
										<strong>   
											Ventas   y   Postventa <br> 
											<a href="https://api.whatsapp.com/send?phone=525568849962 &text=Hola,%20Quiero%20más%20información!" title="Ventas">
												556 884 99 62
											</a>
											|  
											<a href="https://api.whatsapp.com/send?phone=525529660346&text=Hola,%20Quiero%20más%20información!" title="Postventa">
												5529660346
											</a>
										</strong>
									</span>
								</li>
								<li>
									<span>
										<i class="fa fa-whatsapp"></i>
										<strong>   
											Citas   y   Servicio <br> 
											<a href="https://api.whatsapp.com/send?phone=525513338485 &text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">
												5513338485 
											</a>
											|  
											<a href="https://api.whatsapp.com/send?phone=525554186816&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">
												5554186816
											</a>
										</strong>
									</span>
								</li>

								<li><a href="#"><i class="fa fa-envelope"></i>recepcion@hondaatizapan.com</a></li>

							</ul>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Horario de Atención</h3>
							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en FAME Honda Atizapán; te escuchamos y atendemos de manera personalizada. </p>
							<p class="work-time"><span>Lunes - Viernes</span> : 8:00  - 19:00 hrs.</p>
							<p class="work-time"><span>Sábado</span> : 8:00  - 14:00 hrs.</p>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div> 

	<br>

	<?php include('contenido/footer.php'); ?>
</div> 			

</body>
</html>